#include ../umbrella.mk
PACKAGE=gcm
DIST_DIR=dist
EBIN_DIR=ebin
PRIV_DIR=priv
INCLUDE_DIRS=include
DEPS_DIR=deps
DEPS ?=
DEPS_EZ=$(foreach DEP, $(DEPS), $(DEPS_DIR)/$(DEP).ez)
# RABBITMQ_HOME ?= /Users/kwon/temp/rabbitmq_server-3.3.5
RABBITMQ_HOME ?= /Users/kwon/Documents/workspace/svn_naver_dev001/dev001/tags/ebs/release/xpn_server-1.0.0
RELEASABLE=true

all: compile

clean:
	rm -rf $(DIST_DIR)
	rm -rf $(EBIN_DIR)

distclean: clean
	rm -rf $(DEPS_DIR)

package: compile $(DEPS_EZ)
	rm -f $(DIST_DIR)/$(PACKAGE).ez
	mkdir -p $(DIST_DIR)/$(PACKAGE)/$(PRIV_DIR)
	cp -r $(EBIN_DIR) $(DIST_DIR)/$(PACKAGE)
	cp -r $(PRIV_DIR) $(DIST_DIR)/$(PACKAGE)
	$(foreach EXTRA_DIR, $(INCLUDE_DIRS), cp -r $(EXTRA_DIR) $(DIST_DIR)/$(PACKAGE);)
	(cd $(DIST_DIR); zip -x '*.svn' -x '*svn*' -x '*.erl' -x '*git*' -x '*rebar*' -r $(PACKAGE).ez $(PACKAGE);)

install: package
	$(foreach DEP, $(DEPS_EZ), cp $(DEP) $(RABBITMQ_HOME)/plugins;)
	cp $(DIST_DIR)/$(PACKAGE).ez $(RABBITMQ_HOME)/plugins

$(DEPS_DIR):
	./rebar get-deps

$(DEPS_EZ):
	cd $(DEPS_DIR); $(foreach DEP, $(DEPS), zip -x '*.svn' -x '*svn*' -x '*.erl' -x '*git*' -x '*rebar*' -r $(DEP).ez $(DEP);)

compile: $(DEPS_DIR)
	./rebar compile
